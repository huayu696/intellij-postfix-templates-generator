package de.endrullis.idea.postfixtemplatesgenerator

import java.io.File

/**
 * Small tool that counts the number of templates and template rules.
 *
 * @author Stefan Endrullis &lt;stefan@endrullis.de&gt;
 */
object PostfixTemplateCounter extends App {

  val templateDir = new File("./templates/")

  val counts = for (langDir ← templateDir.listFiles(_.isDirectory);
                    file ← langDir.listFiles(_.getName.endsWith(".postfixTemplates")))
  yield getCount(file)

  val count = counts.reduce(_ + _)

  println("  条数 : " + count)

  def getCount(file: File): Count = {
    val lines = PostfixTemplateOverrides.managedList(file)
    Count(lines.count(_.startsWith(".")), lines.count(_.contains("→")))
  }

  case class Count(noTemplates: Int, noRules: Int) {
    def +(that: Count): Count = Count(this.noTemplates + that.noTemplates, this.noRules + that.noRules)
  }
}
